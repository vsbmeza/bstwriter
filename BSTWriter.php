<?php

class BSTWriter {
	/**
	 * Print a binary search tree in order
	 *
	 * @param BST $bst
	 */
	public function writeInOrder(BST $bst) {
		$this->traverseLeft($bst);
		printf("%s\n", $bst->getValue());
		$this->traverseRight($bst);
	}

	private function traverseRight(BST $bst) {
		$this->traverse($bst->getRightChild());
	}

	private function traverseLeft(BST $bst) {
		$this->traverse($bst->getLeftChild());
	}

	private function traverse($bst) {
		if ($bst != null) {
			$this->writeInOrder($bst);
		}
	}
}

?>
