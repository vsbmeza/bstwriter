<?php
require_once "BSTWriter.php";
require_once "BST.php";
require_once "TestBSTData.php";
/**
 * Test that the algorithm works
 */
class BSTWriterTest extends PHPUnit_Framework_TestCase {
	/**
	 * @var BSTWriter
	 */
	protected $object;

	protected function setUp() {
		$this->object = new BSTWriter();
	}

	/**
	 * Test that the writer prints the nodes in order.
	 *
	 * @outputBuffering enabled
	 */
	public function testWriteInOrder() {
		$testBstData = $this->createBST();
		$expected = $testBstData->getExpectedOutput();
		$this->object->writeInOrder($testBstData->getBst());
		$this->expectOutputString($expected);
	}

	/**
	 * Creates a sample BST to use in the tests
	 *
	 * @return TestBSTData
	 */
	private function createBST() {
		$one = new BST(1);
		$four = new BST(4);
		$seven = new BST(7);
		$thirteen = new BST(13);
		$six = new BST(6, $four, $seven);
		$fourteen = new BST(14, $thirteen);
		$three = new BST(3, $one, $six);
		$ten = new BST(10, null, $fourteen);
		$eight = new BST(8, $three, $ten);
		return new TestBSTData($eight, "1\n3\n4\n6\n7\n8\n10\n13\n14\n");
	}

}
