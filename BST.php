<?php
class BST {

	/**
	 * Value of the node
	 *
	 * @var int
	 */
	private $value = null;

	/**
	 * The left child of the node (lower than the value)
	 *
	 * @var BST
	 */
	private $left;

	/**
	 * Right child of the node (greater than the value)
	 *
	 * @var BST
	 */
	private $right;

	/**
	 * Create the node of the BST.
	 * Not checking for validity, for the sake of the task it is considered to
	 * be always valid.
	 *
	 * @param     $value
	 * @param BST $lower
	 * @param BST $higher
	 */
	public function __construct($value, BST $lower = null, BST $higher = null) {
		$this->value = $value;
		$this->left = $lower;
		$this->right = $higher;

	}

	/**
	 * The current value of the node.
	 *
	 * @return int
	 */
	public function getValue() {
		return $this->value;
	}

	/**
	 * Returns the left child of the node.
	 * Can return null as per requested.
	 *
	 * @return BST
	 */
	public function getLeftChild() {
		return $this->left;
	}

	/**
	 * Returns the right child of the node.
	 * Can return null as per requested.
	 *
	 * @return BST
	 */
	public function getRightChild() {
		return $this->right;
	}

}

?>
