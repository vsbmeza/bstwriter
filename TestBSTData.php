<?php
class TestBSTData {

	private $bst;
	private $expectedOutput;

	public function __construct(BST $bsd, $expectedOutput) {
		$this->bst = $bsd;
		$this->expectedOutput = $expectedOutput;
	}

	public function getBst() {
		return $this->bst;
	}

	public function getExpectedOutput() {
		return $this->expectedOutput;
	}

}

?>
