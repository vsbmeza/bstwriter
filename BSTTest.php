<?php
require_once "BST.php";
/**
 * Just a quick proof that the node works as expected
 */
class BSTTest extends PHPUnit_Framework_TestCase {

	/**
	 * @var BST lower value
	 */
	private $left;

	/**
	 * @var BST higher value
	 */
	private $right;

	/**
	 * @var BST class under test
	 */
	private $bst;

	/**
	 * @var int
	 */
	private $value = 1;

	protected function setUp() {
		$this->left = $this->getMockBuilder('BST')->disableOriginalConstructor()
			->getMock();
		$this->right =
			$this->getMockBuilder('BST')->disableOriginalConstructor()
				->getMock();
		$this->bst = new BST($this->value, $this->left, $this->right);
	}

	/**
	 * @covers BST::getValue
	 */
	public function testGetValue() {
		$actual = $this->bst->getValue();
		$this->assertEquals($this->value, $actual);
	}

	/**
	 * @covers BST::getLeftChild
	 */
	public function testGetLeftChild() {
		$actual = $this->bst->getLeftChild();
		$this->assertSame($this->left, $actual);

	}

	/**
	 * @covers BST::getRightChild
	 */
	public function testGetRightChild() {
		$actual = $this->bst->getRightChild();
		$this->assertSame($this->right, $actual);
		;
	}
}
